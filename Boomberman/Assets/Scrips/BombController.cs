using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BombController : MonoBehaviour
{
    [Header("Bomb")]
    public KeyCode inputBombKey = KeyCode.Space;
    public GameObject bombPrefab; 
    public float bombExplosionTime = 3f;
    public int bombAmount = 1;
    private int bombsRemaining;

    [Header("Explosion")]
    public Explosion explosionPrefab;
    public LayerMask backGroundLayerMask;
    public float timeExplosion = 1f;
    public int radiusExplosion = 1;

    [Header("Destructible")]
    public Tilemap destructibleTiles;
    public Destructible destructiblePrefab;
    private void OnEnable()
    {
        bombsRemaining = bombAmount;
    }

    private void Update()
    {
        if (bombsRemaining > 0 && Input.GetKeyDown(inputBombKey))
        {
            StartCoroutine(PlaceBomb());
            
        }
    }

    private IEnumerator PlaceBomb()
    {
        Vector2 position = transform.position;
        position.x = Mathf.Round(position.x);
        position.y = Mathf.Round(position.y);
        GameObject bomb = Instantiate(bombPrefab, position, Quaternion.identity);
        bombsRemaining--;
        yield return new WaitForSeconds(bombExplosionTime);

        position = bomb.transform.position;
        position.x = Mathf.Round(position.x);
        position.y = Mathf.Round(position.y);

        Explosion explosion = Instantiate(explosionPrefab, position, Quaternion.identity);
        explosion.SetActiveRenderer(explosion.start);
        explosion.DestroyAfter(timeExplosion);
        Destroy(explosion.gameObject, timeExplosion);
        Explode(position, Vector2.up, radiusExplosion);
        Explode(position, Vector2.down, radiusExplosion);
        Explode(position, Vector2.left, radiusExplosion);
        Explode(position, Vector2.right, radiusExplosion);

        Destroy(bomb);
        bombsRemaining ++;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Bomb"))
        {
            other.isTrigger = false;
        }
    }

    //su? dung. ham` de. quy
    private void Explode(Vector2 position , Vector2 direction,int length)
    {
        if(length <= 0)
        {
            
            return;
        }
        position += direction;
        if (Physics2D.OverlapBox(position, Vector2.one / 2, backGroundLayerMask))
        {
            ClearDestructible(position);
            return;
        }
        Explosion explosion = Instantiate(explosionPrefab, position, Quaternion.identity);
        explosion.SetActiveRenderer(length > 1 ? explosion.middle : explosion.end);      
        explosion.SetDirection(direction);
        explosion.DestroyAfter(timeExplosion);
        Explode(position, direction, length - 1);
    }

    private void ClearDestructible(Vector2 position)
    {
        Vector3Int cell = destructibleTiles.WorldToCell(position);
        TileBase tiles = destructibleTiles.GetTile(cell);
        if(tiles != null)
        {
            Instantiate(destructiblePrefab, position, Quaternion.identity);
            destructibleTiles.SetTile(cell, null);
        }
        Debug.Log("aaaa");
    }

    public void AddBomb()
    {
        bombAmount++;
        bombsRemaining++;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    public float destructibleTime = 1f;

    [Range(0f,1f)]
    public float itemSpawnChange = 0.2f;
    public GameObject[] spawnItems;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, destructibleTime);
    }

    private void OnDestroy()
    {
        if(spawnItems.Length > 0 && Random.value < itemSpawnChange)
        {
            int randomIndex = Random.Range(0, spawnItems.Length);
            Instantiate(spawnItems[randomIndex], transform.position, Quaternion.identity);
        }
    }

}

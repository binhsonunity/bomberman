using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D rb2d { get; private set; }
    private Vector2 direction;
    public float speed = 5f;
    
    public KeyCode inputUp = KeyCode.W;
    public KeyCode inputDown = KeyCode.S;
    public KeyCode inputLeft = KeyCode.A;
    public KeyCode inputRight = KeyCode.D;

    public AnimationController animationMoveUp;
    public AnimationController animationMoveDown;
    public AnimationController animationMoveLeft;
    public AnimationController animationMoveRight;
    public AnimationController animationDeath;

    private AnimationController activeSpriteAnimation;


    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        activeSpriteAnimation = animationMoveDown;

    }
    private void Update()
    {
        if (Input.GetKey(inputUp))
        {
            SetDirection(Vector2.up, animationMoveUp);         
        }
        else if (Input.GetKey(inputDown))
        {
            SetDirection(Vector2.down, animationMoveDown);         
        }
        else if (Input.GetKey(inputLeft))
        {
            SetDirection(Vector2.left, animationMoveLeft);          
        }
        else if (Input.GetKey(inputRight))
        {
            SetDirection(Vector2.right, animationMoveRight); 
        }
        else
        {
            SetDirection(Vector2.zero, activeSpriteAnimation);
        }
    }

    private void FixedUpdate()
    {
        Vector2 position = rb2d.position;
        Vector2 translation = speed * direction * Time.fixedDeltaTime;
        rb2d.MovePosition(position + translation);
    }

    private void SetDirection (Vector2 newDirection,AnimationController animationSprite)
    {
        direction = newDirection;
        animationMoveUp.enabled = animationSprite == animationMoveUp;
        animationMoveDown.enabled = animationSprite == animationMoveDown;
        animationMoveLeft.enabled = animationSprite == animationMoveLeft;
        animationMoveRight.enabled = animationSprite == animationMoveRight;
        activeSpriteAnimation = animationSprite;
        activeSpriteAnimation.idle = direction == Vector2.zero;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Explosion"))
        {
            DeathSequence();
        }      
    }

    private void DeathSequence()
    {
        enabled = false;
        GetComponent<BombController>().enabled = false;
        animationMoveUp.enabled = false;
        animationMoveDown.enabled = false;
        animationMoveLeft.enabled = false;
        animationMoveRight.enabled = false;
        animationDeath.enabled = true;

        Invoke(nameof(OnDeathSequenceEnded), 1.25f);
    }

    private void OnDeathSequenceEnded()
    {
        gameObject.SetActive(false);
    }
}
